package com.zoomonit.tools.jmxadmin.configuration;

import java.util.HashMap;
import java.util.Map;

public class JMXAdminConfiguration {

    static final transient String DEFAULT_LOCAL_SERVER_ALIAS = "local";
    private static final String DEFAULT_LOCAL_SERVER_URL = "service:jmx:rmi:///jndi/rmi://localhost/jmxrmi";

    private boolean activateLocalServer = true;

    private String localServerAlias = DEFAULT_LOCAL_SERVER_ALIAS;

    private JMXAdminPoolConfiguration poolConfiguration = new JMXAdminPoolConfiguration();

    private Map<String, JMXServerConfiguration> aliasToServerConfigurationMap = new HashMap<>();


    public boolean isActivateLocalServer() {
        return activateLocalServer;
    }

    public void setActivateLocalServer(boolean activateLocalServer) {
        this.activateLocalServer = activateLocalServer;
    }

    public void setLocalServerAlias(String localServerAlias) {
        this.localServerAlias = localServerAlias;
    }

    public JMXAdminPoolConfiguration getPoolConfiguration() {
        return poolConfiguration;
    }

    public Map<String, JMXServerConfiguration> getAliasToServerConfigurationMap() {
        return aliasToServerConfigurationMap;
    }

    void validate() throws JMXAdminConfigurationException {
        for (JMXServerConfiguration serverConfiguration : aliasToServerConfigurationMap.values()) {
            serverConfiguration.validate();
        }
    }

    void initialize() {
        if (activateLocalServer) {
            JMXServerConfiguration serverConfiguration = new JMXServerConfiguration();
            serverConfiguration.setAlias(localServerAlias);
            serverConfiguration.setJmxUrl(DEFAULT_LOCAL_SERVER_URL);
            aliasToServerConfigurationMap.put(localServerAlias, serverConfiguration);
        }
    }

    public String getLocalServerAlias() {
        return localServerAlias;
    }
}
