package com.zoomonit.tools.jmxadmin.configuration;

import java.util.Arrays;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class DefaultJMXAdminConfigurator implements JMXAdminConfigurator {

    private JMXAdminConfiguration configuration = new JMXAdminConfiguration();


    @Override
    public JMXAdminConfigurator activateLocalServer() {
        return activateLocalServer(JMXAdminConfiguration.DEFAULT_LOCAL_SERVER_ALIAS);
    }

    @Override
    public JMXAdminConfigurator activateLocalServer(String serverAlias) {
        configuration.setActivateLocalServer(true);
        configuration.setLocalServerAlias(serverAlias);
        addServer(serverAlias, "service:jmx:rmi:///jndi/rmi://localhost/jmxrmi");
        return this;
    }

    @Override
    public JMXAdminConfigurator addServer(String serverAlias, String jmxUrl) {
        return addServer(serverAlias, jmxUrl, null, null, false);
    }

    @Override
    public JMXAdminConfigurator addServer(String serverAlias, String jmxUrl, String username, String password) {
        return addServer(serverAlias, jmxUrl, username, password, false);
    }

    @Override
    public JMXAdminConfigurator addServer(String serverAlias, String jmxUrl, boolean isSecured) {
        return addServer(serverAlias, jmxUrl, null, null, isSecured);
    }

    @Override
    public JMXAdminConfigurator addServer(String serverAlias, String jmxUrl, String username, String password, boolean isSecured) {
        JMXServerConfiguration serverConfiguration = getJmxServerConfiguration(serverAlias);
        serverConfiguration.setJmxUrl(jmxUrl);
        serverConfiguration.setUsername(username);
        serverConfiguration.setPassword(password);
        serverConfiguration.setSecured(isSecured);
        putInConfiguration(serverAlias, serverConfiguration);
        return this;
    }

    private JMXServerConfiguration getJmxServerConfiguration(String serverAlias) {
        JMXServerConfiguration serverConfiguration;
        Map<String, JMXServerConfiguration> aliasToServerConfigurationMap = configuration.getAliasToServerConfigurationMap();
        if (aliasToServerConfigurationMap.containsKey(serverAlias)) {
            serverConfiguration = aliasToServerConfigurationMap.get(serverAlias);
        } else {
            serverConfiguration = new JMXServerConfiguration();
            serverConfiguration.setAlias(serverAlias);
            putInConfiguration(serverAlias, serverConfiguration);
        }
        return serverConfiguration;
    }

    @Override
    public JMXAdminConfigurator deactivateServer(String serverAlias) {
        JMXServerConfiguration jmxServerConfiguration = getJmxServerConfiguration(serverAlias);
        jmxServerConfiguration.setActive(false);
        return this;
    }

    private void putInConfiguration(String serverAlias, JMXServerConfiguration jmxServerConfiguration) {
        configuration.getAliasToServerConfigurationMap().put(serverAlias, jmxServerConfiguration);
    }

    @Override
    public JMXAdminConfigurator includeObjectNames(String serverAlias, String... objectNames) {
        JMXServerConfiguration jmxServerConfiguration = getJmxServerConfiguration(serverAlias);
        jmxServerConfiguration.setIncludedObjectNames(Arrays.asList(objectNames));
        return this;
    }

    @Override
    public JMXAdminConfigurator excludeObjectNames(String serverAlias, String... objectNames) {
        JMXServerConfiguration jmxServerConfiguration = getJmxServerConfiguration(serverAlias);
        jmxServerConfiguration.setExcludedObjectNames(Arrays.asList(objectNames));
        return this;
    }

    public JMXAdminConfiguration getConfiguration() {
        configuration.initialize();
        return configuration;
    }

}
