package com.zoomonit.tools.jmxadmin.view;

/**
 * Created by oaouattara on 20/05/16.
 */
public class JmxAttribute {

    private String name;

    private Object value;

    public JmxAttribute(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }



}
