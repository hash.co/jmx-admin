package com.zoomonit.tools.jmxadmin.view;

import java.util.List;
import java.util.SortedMap;

/**
 * Created by oaouattara on 20/05/16.
 */
public class JmxServerDescription {

    private String serverName;
    private SortedMap<String, List<String>> domainsToMbeanName;

    public JmxServerDescription withServerName(String serverName) {
        this.serverName = serverName;
        return this;
    }

    public JmxServerDescription withDomainsToMBeanName(SortedMap<String, List<String>> domainsToMbeanName) {
        this.domainsToMbeanName = domainsToMbeanName;
        return this;
    }

    public String getServerName() {
        return serverName;
    }

    public SortedMap<String, List<String>> getDomainsToMbeanName() {
        return domainsToMbeanName;
    }
}
