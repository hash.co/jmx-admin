package com.zoomonit.tools.jmxadmin;

import javax.management.remote.JMXConnector;

import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

class JmxPool extends GenericObjectPool<JMXConnector> {

    JmxPool(JmxPoolFactory factory, GenericObjectPoolConfig config) {
        super(factory, config);
    }
}