package com.zoomonit.tools.jmxadmin.view;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/META-INF/spring/applicationContext.xml", "classpath:/META-INF/spring/webmvc-config.xml"})
@WebAppConfiguration
@Transactional
@Rollback
public class JMXRouterControllerTest {

    @Autowired
    private WebApplicationContext applicationContext;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        mockMvc = webAppContextSetup(applicationContext).build();
    }

    @Test
    public void getAllMBeanDescriptionOnComponentServer() throws Exception {
        mockMvc.perform(get("/jmx/component1/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.serverName",is("Component 1")))
                .andExpect(jsonPath("$.domainsToMbeanName.*").isArray())
                .andExpect(jsonPath("$.domainsToMbeanName.['java.lang']").exists());
    }

    @Test
    public void getMBeanDescriptionOnComponentServer() throws Exception {
        mockMvc.perform(get("/jmx/component1/1/java.lang:type=Memory")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.objectName",is("java.lang:type=Memory")))
                .andExpect(jsonPath("$.beanInfo.attributes").isArray())
                .andExpect(jsonPath("$.beanInfo.operations").isArray())
                .andExpect(jsonPath("$.beanInfo.operations[0].name", is("gc")));

    }

    @Test
    public void callJMXMethodOnComponentServer() throws Exception {

        mockMvc.perform(post("/jmx/component1/1/java.lang:type=Memory/gc")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content("{}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", nullValue()));
    }

    @Test
    public void getJMXInfoOnComponentServer() throws Exception {
        mockMvc.perform(get("/jmx/component1/1/java.lang:type=Memory/Verbose")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name",is("Verbose")))
                .andExpect(jsonPath("$.value",is(false)));
    }

}