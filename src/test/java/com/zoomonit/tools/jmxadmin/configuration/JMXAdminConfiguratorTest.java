package com.zoomonit.tools.jmxadmin.configuration;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Test;

public class JMXAdminConfiguratorTest {

    @Test
    public void activateLocalServer() throws Exception {
        DefaultJMXAdminConfigurator jmxAdminConfigurator = new DefaultJMXAdminConfigurator();
        jmxAdminConfigurator.activateLocalServer();
        JMXAdminConfiguration configuration = jmxAdminConfigurator.getConfiguration();
        assertTrue(configuration.isActivateLocalServer());
    }

    @Test
    public void activateLocalServer1() throws Exception {
        DefaultJMXAdminConfigurator jmxAdminConfigurator = new DefaultJMXAdminConfigurator();
        jmxAdminConfigurator.activateLocalServer("AltName");
        JMXAdminConfiguration configuration = jmxAdminConfigurator.getConfiguration();
        assertTrue(configuration.isActivateLocalServer());
        assertEquals("AltName", configuration.getLocalServerAlias());
    }

    @Test
    public void addServer() throws Exception {
        DefaultJMXAdminConfigurator jmxAdminConfigurator = new DefaultJMXAdminConfigurator();
        jmxAdminConfigurator.addServer("myserver1", "url1");
        JMXAdminConfiguration configuration = jmxAdminConfigurator.getConfiguration();
        assertTrue(configuration.isActivateLocalServer());
        Map<String, JMXServerConfiguration> aliasToServerConfigurationMap = configuration.getAliasToServerConfigurationMap();
        assertEquals(2, aliasToServerConfigurationMap.size());
        JMXServerConfiguration serverConfiguration = aliasToServerConfigurationMap.get("myserver1");
        assertEquals("url1", serverConfiguration.getJmxUrl());
        assertEquals("myserver1", serverConfiguration.getAlias());
        assertNull(serverConfiguration.getUsername());
        assertNull(serverConfiguration.getPassword());
        assertFalse(serverConfiguration.isSecured());
        assertTrue(serverConfiguration.isActive());
    }

    @Test
    public void addServer1() throws Exception {
        DefaultJMXAdminConfigurator jmxAdminConfigurator = new DefaultJMXAdminConfigurator();
        jmxAdminConfigurator.addServer("myserver1", "url1", true);
        JMXAdminConfiguration configuration = jmxAdminConfigurator.getConfiguration();
        assertTrue(configuration.isActivateLocalServer());
        Map<String, JMXServerConfiguration> aliasToServerConfigurationMap = configuration.getAliasToServerConfigurationMap();
        assertEquals(2, aliasToServerConfigurationMap.size());
        JMXServerConfiguration serverConfiguration = aliasToServerConfigurationMap.get("myserver1");
        assertEquals("url1", serverConfiguration.getJmxUrl());
        assertEquals("myserver1", serverConfiguration.getAlias());
        assertNull(serverConfiguration.getUsername());
        assertNull(serverConfiguration.getPassword());
        assertTrue(serverConfiguration.isSecured());
        assertTrue(serverConfiguration.isActive());
    }

    @Test
    public void addServer2() throws Exception {
        DefaultJMXAdminConfigurator jmxAdminConfigurator = new DefaultJMXAdminConfigurator();
        jmxAdminConfigurator.addServer("myserver1", "url1", "user","pass");
        JMXAdminConfiguration configuration = jmxAdminConfigurator.getConfiguration();
        assertTrue(configuration.isActivateLocalServer());
        Map<String, JMXServerConfiguration> aliasToServerConfigurationMap = configuration.getAliasToServerConfigurationMap();
        assertEquals(2, aliasToServerConfigurationMap.size());
        JMXServerConfiguration serverConfiguration = aliasToServerConfigurationMap.get("myserver1");
        assertEquals("url1", serverConfiguration.getJmxUrl());
        assertEquals("myserver1", serverConfiguration.getAlias());
        assertEquals("user", serverConfiguration.getUsername());
        assertEquals("pass", serverConfiguration.getPassword());
        assertFalse(serverConfiguration.isSecured());
        assertTrue(serverConfiguration.isActive());
    }

    @Test
    public void addServer3() throws Exception {
        DefaultJMXAdminConfigurator jmxAdminConfigurator = new DefaultJMXAdminConfigurator();
        jmxAdminConfigurator.addServer("myserver1", "url1", "user","pass", true);
        JMXAdminConfiguration configuration = jmxAdminConfigurator.getConfiguration();
        assertTrue(configuration.isActivateLocalServer());
        Map<String, JMXServerConfiguration> aliasToServerConfigurationMap = configuration.getAliasToServerConfigurationMap();
        assertEquals(2, aliasToServerConfigurationMap.size());
        JMXServerConfiguration serverConfiguration = aliasToServerConfigurationMap.get("myserver1");
        assertEquals("url1", serverConfiguration.getJmxUrl());
        assertEquals("myserver1", serverConfiguration.getAlias());
        assertEquals("user", serverConfiguration.getUsername());
        assertEquals("pass", serverConfiguration.getPassword());
        assertTrue(serverConfiguration.isActive());
        assertTrue(serverConfiguration.isSecured());
    }

    @Test
    public void deactivateServer() {
        DefaultJMXAdminConfigurator jmxAdminConfigurator = new DefaultJMXAdminConfigurator();
        jmxAdminConfigurator.addServer("myserver", "myurl").deactivateServer("myserver");
        JMXServerConfiguration serverConfiguration = jmxAdminConfigurator.getConfiguration().getAliasToServerConfigurationMap().get("myserver");
        assertFalse(serverConfiguration.isActive());
    }

    @Test
    public void includeObjectNames() {
        DefaultJMXAdminConfigurator jmxAdminConfigurator = new DefaultJMXAdminConfigurator();
        jmxAdminConfigurator.addServer("myserver", "myurl")
                .includeObjectNames("myserver", "domain:*", "domain1:*");
        JMXServerConfiguration serverConfiguration = jmxAdminConfigurator.getConfiguration().getAliasToServerConfigurationMap().get("myserver");
        assertEquals(2, serverConfiguration.getIncludedObjectNames().size());
        assertTrue(serverConfiguration.getExcludedObjectNames().isEmpty());
    }

    @Test
    public void excludeObjectNames() {
        DefaultJMXAdminConfigurator jmxAdminConfigurator = new DefaultJMXAdminConfigurator();
        jmxAdminConfigurator.addServer("myserver", "myurl")
                .excludeObjectNames("myserver", "domain:*", "domain1:*");
        JMXServerConfiguration serverConfiguration = jmxAdminConfigurator.getConfiguration().getAliasToServerConfigurationMap().get("myserver");
        assertEquals(2, serverConfiguration.getExcludedObjectNames().size());
        assertTrue(serverConfiguration.getIncludedObjectNames().isEmpty());
    }

}